package com.ilmirius;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class MoveAdapterTest {
    @Mock
    UObject uObject = new UObject();
    @InjectMocks
    MoveAdapter moveAdapter = new MoveAdapter(uObject);

    @Test(expected = IllegalArgumentException.class)
    public void testGetPositionExpectedException() {
        moveAdapter.getPosition();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetPosition() {
        moveAdapter.setPosition(new Vector(new int[]{1, -3}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetVelocityExpectedException() {
        moveAdapter.getVelocity();
    }
}
