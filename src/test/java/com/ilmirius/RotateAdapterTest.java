package com.ilmirius;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class RotateAdapterTest {
    @Mock
    UObject uObject = new UObject();
    @InjectMocks
    RotateAdapter rotateAdapter = new RotateAdapter(uObject);

    @Test(expected = IllegalArgumentException.class)
    public void testGetDirectionExpectedException() {
        rotateAdapter.getDirection();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetDirectionExpectedException() {
        rotateAdapter.setDirection(-10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAngularVelocityExpectedException() {
        rotateAdapter.getAngularVelocity();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMaxDirectionsExpectedException() {
        rotateAdapter.getMaxDirections();
    }
}
