package com.ilmirius;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class MoveCommandTest {
    @Mock
    Movable movable;
    @InjectMocks
    MoveCommand moveCommand;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecute() throws Exception {
        when(movable.getPosition()).thenReturn(new Vector(new int[]{12, 5}));
        when(movable.getVelocity()).thenReturn(new Vector(new int[]{-7, 3}));

        moveCommand.execute();
        Mockito.verify(movable, times(1)).getPosition();
    }

    @Test
    public void test() {
        UObject tank = new Tank();
        Vector basePosition = new Vector(new int[]{12, 5});
        Vector baseVelocity = new Vector(new int[]{-7, 3});
        tank.setProperty("position", basePosition);
        tank.setProperty("velocity", baseVelocity);
        Command moveCommand = new MoveCommand(new MoveAdapter(tank));
        moveCommand.execute();
        Assertions.assertThat(new Vector(new int[]{5, 8})).isEqualToComparingFieldByField(tank.getProperty("position"));
    }
}