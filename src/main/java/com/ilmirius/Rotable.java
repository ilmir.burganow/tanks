package com.ilmirius;

public interface Rotable {

    int getDirection();

    void setDirection(int direction);

    int getAngularVelocity();

    int getMaxDirections();

}
