package com.ilmirius;

public class MoveCommand implements Command {

    final Movable movable;

    public MoveCommand(Movable movable) {
        this.movable = movable;
    }

    public void execute() {
        Vector newPosition = Vector.plus(movable.getPosition(), movable.getVelocity());
        movable.setPosition(newPosition);
    }

}
