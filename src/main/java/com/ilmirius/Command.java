package com.ilmirius;

public interface Command {

    void execute();
}
