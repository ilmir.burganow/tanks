package com.ilmirius;

import java.util.HashMap;
import java.util.Map;

public class UObject {

    final Map<String, Object> propertyMap = new HashMap<>();

    public Object getProperty(String key) {
        Object obj = propertyMap.get(key);
        if (obj == null) {
            throw new IllegalArgumentException();
        }
        return obj;
    }

    public void setProperty(String key, Object value) {
        propertyMap.put(key, value);
    }
}
