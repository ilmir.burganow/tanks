package com.ilmirius;

public class RotateAdapter implements Rotable {

    UObject uObject;

    public RotateAdapter(UObject uObject) {
        this.uObject = uObject;
    }

    @Override
    public int getDirection() {
        return (int) uObject.getProperty("direction");
    }

    @Override
    public void setDirection(int direction) {
        if (direction < 0) {
            throw new IllegalArgumentException();
        }
        uObject.setProperty("direction", direction);
    }

    @Override
    public int getAngularVelocity() {
        return (int) uObject.getProperty("angularVelocity");
    }

    @Override
    public int getMaxDirections() {
        return (int) uObject.getProperty("maxDirections");
    }
}
