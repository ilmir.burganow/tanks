package com.ilmirius;

public class Main {

    public static void main(String[] args) {

        UObject tank = new Tank();
        Vector basePosition = new Vector(new int[]{0, 0});
        Vector baseVelocity = new Vector(new int[]{1, 2});
        tank.setProperty("position", basePosition);
        tank.setProperty("velocity", baseVelocity);
        System.out.println("Base position: " + tank.getProperty("position"));
        Command moveCommand = new MoveCommand(new MoveAdapter(tank));
        moveCommand.execute();
        System.out.println("After position: " + tank.getProperty("position"));


        tank.setProperty("direction", 3);
        tank.setProperty("angularVelocity", 10);
        tank.setProperty("maxDirections", 12);
        Command rotateCommand = new RotateCommand(new RotateAdapter(tank));
        rotateCommand.execute();

        System.out.println("New directon: " + tank.getProperty("direction"));

    }


}
