package com.ilmirius;

public interface Movable {

    Vector getPosition();

    void setPosition(Vector vector);

    Vector getVelocity();
}
