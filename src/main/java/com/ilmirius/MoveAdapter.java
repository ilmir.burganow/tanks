package com.ilmirius;

public class MoveAdapter implements Movable {

    final UObject uObject;

    public MoveAdapter(UObject uObject) {
        this.uObject = uObject;
    }

    @Override
    public Vector getPosition() {
        return (Vector) uObject.getProperty("position");
    }

    @Override
    public void setPosition(Vector vector) {
        for (int i : vector.body) {
            if (i < 0) {
                throw new IllegalArgumentException();
            }
        }
        uObject.setProperty("position", vector);
    }

    @Override
    public Vector getVelocity() {
        return (Vector) uObject.getProperty("velocity");
    }
}
