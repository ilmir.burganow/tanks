package com.ilmirius;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Arrays;

@EqualsAndHashCode
public class Vector {

    final int[] body;

    public Vector(int[] body) {
        this.body = body;
    }

    public static Vector plus(Vector v1, Vector v2) {
        int[] newBody = new int[v1.body.length];
        for (int i = 0; i < v1.body.length; i++) {
            newBody[i] = v1.body[i] + v2.body[i];
        }

        return new Vector(newBody);
    }

    @Override
    public String toString() {
        return "Vector{" +
                "body=" + Arrays.toString(body) +
                '}';
    }
}
